require "./datastore"
require "./event_queue"
require "./event_enqueuer"
require "./event_writer"

module TacScribe
  class Daemon
    def initialize(@tacview_host : String,
                   @tacview_port : Int32,
                   @tacview_client_name : String,
                   tacview_password : String,
                   @db_host : String,
                   @db_port : Int32,
                   @db_name : String,
                   @db_username : String,
                   @db_password : String,
                   whitelist : String)
      puts "#{Time.utc} TacScribe Daemon: Initializing"
      @tacview_password = tacview_password.empty? ? nil : tacview_password
      @whitelist = Set(String).new
      unless whitelist.empty?
        @whitelist.concat(File.read(whitelist).split)
      end
    end

    def start_processing
      puts "#{Time.utc} TacScribe Daemon: Starting"
      loop do
        puts "#{Time.utc} TacScribe Daemon: Configuring"

        event_input_channel = EventChannel.new(1024)
        event_output_channel = EventChannel.new

        event_queue = EventQueue.new(event_input_channel, event_output_channel, @whitelist)

        client = TacviewClient::Client.new(
          processor: enqueuer = TacScribe::EventEnqueuer.new(event_input_channel),
          host: @tacview_host,
          port: @tacview_port,
          password: @tacview_password,
          client_name: @tacview_client_name
        )

        datastore = TacScribe::Datastore.new

        datastore.configure do |config|
          config.host = @db_host
          config.port = @db_port
          config.database = @db_name
          config.username = @db_username
          config.password = @db_password
        end

        datastore.clear

        writer = TacScribe::EventWriter.new(event_output_channel, datastore)

        puts "#{Time.utc} TacScribe Daemon: Connecting"
        begin
          event_queue.start
          writer.start
          client.connect
          Fiber.yield
        rescue ex
          puts "#{Time.utc} TacScribe Daemon: #{ex.message}"
        end
        puts "#{Time.utc} TacScribe Daemon: Stopping"
        event_queue.stop
        sleep 5
        puts "#{Time.utc} TacScribe Daemon: Stopped"
        sleep 25
        puts "#{Time.utc} TacScribe Daemon: Restarting"
      end
    end
  end
end
