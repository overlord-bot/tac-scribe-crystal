module TacScribe
  class UnitUpdateEvent
    getter :object, :time

    def initialize(@object : Hash(String, String | BigDecimal), @time : Time)
    end
  end

  class UnitDeletionEvent
    getter :object_id

    def initialize(@object_id : String)
    end
  end

  class ReferenceLatitudeSettingEvent
    getter :value

    def initialize(@value : BigDecimal)
    end
  end

  class ReferenceLongitudeSettingEvent
    getter :value

    def initialize(@value : BigDecimal)
    end
  end

  alias EventChannel = Channel(UnitUpdateEvent |
                               UnitDeletionEvent |
                               ReferenceLatitudeSettingEvent |
                               ReferenceLongitudeSettingEvent)

  class EventQueue
    def initialize(@input_channel : EventChannel,
                   @output_channel : EventChannel,
                   @whitelist : Set(String))
      @queue = Deque(UnitUpdateEvent |
                     UnitDeletionEvent |
                     ReferenceLatitudeSettingEvent |
                     ReferenceLongitudeSettingEvent).new(1024)
      @inputs = 0
      @outputs = 0
      @dropped = 0
      @passed = 0
      @stop = false
      @blacklisted_objects = Set(String).new
    end

    def start
      spawn name: "EventQueue status reporting" do
        loop do
          break if @stop
          sleep 5
          if @whitelist.empty?
            puts "#{Time.utc}\tEvents In: #{@inputs}\tOut: #{@outputs}\tBacklog: #{@queue.size}" unless @stop
          else
            puts "#{Time.utc}\tEvents In: #{@inputs}\tDrop: #{@dropped}\tPass: #{@passed}\tOut: #{@outputs}\tBacklog: #{@queue.size}\tBlacklist: #{@blacklisted_objects.size}" unless @stop
          end
          @inputs = 0
          @outputs = 0
          @dropped = 0
          @passed = 0
        end
      end

      spawn name: "EventQueue input channel processing" do
        loop do
          event = @input_channel.receive
          @inputs += 1
          if @whitelist.empty?
            @queue.push event
            @inputs += 1
          elsif event.class == UnitUpdateEvent
            event = event.as(UnitUpdateEvent)
            if event.object.has_key?("type")
              if @whitelist.includes?(event.object["type"])
                @queue.unshift event
                @passed += 1
              else
                @blacklisted_objects << event.object["object_id"].as(String)
                @dropped += 1
              end
            else
              if @blacklisted_objects.includes?(event.object["object_id"].as(String))
                @dropped += 1
              else
                @queue.push event
                @passed += 1
              end
            end
          elsif event.class == UnitDeletionEvent
            event = event.as(UnitDeletionEvent)
            if @blacklisted_objects.includes?(event.object_id)
              @dropped += 1
              @blacklisted_objects.delete(event.object_id)
            else
              @queue.push event
              @passed += 1
            end
          else
            @queue.push event
            @passed += 1
          end
        end
      rescue Channel::ClosedError
        puts "#{Time.utc} EventQueue: Input Channel closed"
      end

      spawn name: "EventQueue output channel processing" do
        loop do
          event = @queue.shift do
            sleep 0.1
            nil
          end
          @output_channel.send event if event
          @outputs += 1 if event
        end
      rescue Channel::ClosedError
        puts "#{Time.utc} EventQueue: Output Channel closed"
      end
    end

    def stop
      @stop = true
      @input_channel.close
      @output_channel.close
      @queue.clear
      puts "#{Time.utc} EventQueue: Stopped"
    end
  end
end
