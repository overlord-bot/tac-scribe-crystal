require "db"
require "pg"

module TacScribe
  class Datastore
    property db
    property configuration = Configuration.new

    def configure
      yield self.configuration
    end

    def upsert_object(cache_object : Hash(String, String | BigDecimal)) : Nil
      coalition = if cache_object.has_key?("coalition")
                    cache_object["coalition"] == "Allies" ? 0 : 1
                  else
                    2
                  end

      upsert_command = ""
      begin
        upsert_command = "INSERT INTO units (id, \"position\", altitude, type, name, pilot, \"group\", coalition, heading , updated_at)
                 VALUES
                 (
                   '#{cache_object["object_id"]}',
                   ST_SetSRID( ST_Point( #{cache_object["longitude"]}, #{cache_object["latitude"]}), 4326)::geography,
                   #{cache_object.fetch("altitude", 0.00).to_f},
                   '#{cache_object["type"]}',
                   '#{cache_object.fetch("name", nil)}',
                   '#{cache_object.fetch("pilot", nil)}',
                   '#{cache_object.fetch("group", nil)}',
                   #{coalition},
                   #{cache_object.fetch("heading", -1)},
                   '#{cache_object.fetch("updated_at", Time.utc)}'
                 )
                 ON CONFLICT (id)
                 DO UPDATE
                   SET \"position\" = ST_SetSRID( ST_Point( #{cache_object["longitude"]}, #{cache_object["latitude"]}), 4326)::geography,
                   altitude = #{cache_object.fetch("altitude", 0.00).to_f},
                   heading = #{cache_object.fetch("heading", 0)},
                   updated_at = '#{cache_object.fetch("updated_at", Time.utc)}';"
      rescue ex
        puts ex.message
        puts cache_object
      end

      DB.open(connection_string) do |db|
        db.exec upsert_command
      end
    end

    def delete_object(object_id : String) : Nil
      DB.open(connection_string) do |db|
        db.exec "DELETE FROM units WHERE id = '#{object_id}'"
      end
    end

    def clear : Nil
      DB.open(connection_string) do |db|
        db.exec "TRUNCATE TABLE units"
      end
    end

    private def connection_string
      @connection_string ||= "postgres://#{@configuration.username}:#{@configuration.password}@" \
                             "#{@configuration.host}:#{@configuration.port}" \
                             "/#{@configuration.database}"
    end

    # Contains all the connection configuration required for connectin
    # to a postgresql database. Defaults to localhost with ubuntu presets
    class Configuration
      property host : String = "localhost"
      property port : Int32 = 5432
      property database : String = "tac_scribe"
      property username : String = "tac_scribe"
      property password : String = "tac_scribe"
    end
  end
end
